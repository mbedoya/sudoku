var sudokuValidator = function (board) {
    var validElements = "123456789";

    var groupIsCorrect = function (array) {
        return array.slice().sort().join("") === validElements;
    }

    var rowsAreValid = function () {
        return board
            .map((row) => groupIsCorrect(row))
            .filter((validRow) => validRow).length === 9
    }

    var colsAreValid = function () {
        for (let i = 0; i < 9; i++) {
            if (!groupIsCorrect(board.map(row => row[i]))) {
                return false;
            }
        }
        return true;
    }

    var blocksAreValid = function () {
        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                if (
                    !groupIsCorrect(
                        board[i * 3]
                            .slice(j * 3, j * 3 + 3)
                            .concat(
                            board[i * 3 + 1].slice(j * 3, j * 3 + 3),
                            board[i * 3 + 2].slice(j * 3, j * 3 + 3)
                            )
                    )
                ) {
                    return false;
                }
            }
        }

        return true;
    }

    return rowsAreValid() && colsAreValid() && blocksAreValid();
}

var sudoku = {
    validateInput: sudokuValidator
};

//exports.sudoku = sudoku;